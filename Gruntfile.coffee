module.exports = (grunt) ->

  grunt.initConfig
    sass:
      dist:
        options:
          style: 'expanded'
        files:
          'dist/css/styles.css': 'src/css/styles.sass',

    coffee:
      compile:
        files:
          'dist/js/script.js': 'src/js/script.coffee',

    haml:
      dist:
        files:
          'dist/index.html': 'src/index.haml'

    uglify:
      vendor:
        files:
          'dist/js/vendor.min.js': ['src/js/vendor/lazysizes.min.js', 'src/js/vendor/ls.unveilhooks.min.js']

    copy:
      files:
        cwd: 'src/fonts/',
        src: '**/*',
        dest: 'dist/fonts',
        expand: true

    imagemin:
      files:
        expand: true,
        cwd: 'src/img',
        src: ['**/*.{png,jpg,gif,svg}'],
        dest: 'dist/img'

    watch:
      scripts:
        files: 'src/**/*.coffee',
        tasks: ['coffee']
      styles:
        files: 'src/**/*.sass',
        tasks: ['sass']
      html:
        files: 'src/**/*.haml',
        tasks: ['haml']
      img:
        files: 'src/**/*.img',
        tasks: ['imagemin']

  grunt.loadNpmTasks 'grunt-contrib-sass'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-haml'
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks 'grunt-contrib-imagemin'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  # Default task(s).
  grunt.registerTask 'default', ['sass', 'coffee', 'haml', 'uglify', 'copy', 'imagemin', 'watch']