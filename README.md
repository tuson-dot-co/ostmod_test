# Joe Tuson | Ostmodern Test

### Building the test:

As you can see - this is a very simple project structure using node. To compile the test, run:
```sh
$ npm install
```
And once that finishes, run:
```sh
$ grunt
```

This should build the test into a ***/dist*** folder. There, you will find ***index.html***

Enjoy!

### Built with:
- [HAML] - for markup 
- [SASS] - for styling
- [Grunt] - for task automation/compilation
- [lazysizes] - for lazyloading

[//]: # (Reference links:)
   [HAML]: <http://haml.info/>
   [SASS]: <http://sass-lang.com/>
   [Grunt]: <http://gruntjs.com/>
   [lazysizes]: <https://github.com/aFarkas/lazysizes>

NOTE!
Browser Ad Blocker extensions will block the ad element. Just sayin...